**:warning: This project was moved to https://github.com/bolov/dotfiles**

---

Linux - Configuration files, Settings, How To
=============================================

[Linux configuration files](doc/config-files.markdown)

[Linux: Settings, How To](doc/linux.markdown)

